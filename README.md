# Workzone OpenAPI spec
Workzone REST API openapi spec can be found here https://bitbucket.org/izymessupport/workzone-public-apis/src/master/workzone-api-generated.yaml

# Overview Workzone REST API - Repository and Project Configuration

To discover and use the workzone REST API follow this pattern:

## Reviewers

### GET 
**Repository**: GET http://localhost:7990/rest/workzoneresource/latest/branch/reviewers/{PROJ}/{repo} and check the JSON data. 

**Project**: GET http://localhost:7990/rest/workzoneresource/latest/project/{PROJ}/reviewers and check the JSON data. 

```
[
  {
    "projectKey": "PROJ",
    "repoSlug": "repo",
    "refName": "refs/heads/develop",
    "users": [
      {
        "name": "agrant",
….
  },
  {
    "projectKey": "PROJ",
    "repoSlug": "repo",
    "refName": "refs/heads/master",
    "users": [
      
    ],
    "groups": [
      
    ],
    "mandatoryUsers": [
      {
        "name": "emurphy",
….
```

 It’s an array where the refName (branch name), projectKey and repoSlug forms the ‘key’ under which the config is stored.

### POST
To store a config for a project/repo/branch POST a **single** array entry per branch.

**Repository**: POST http://localhost:7990/rest/workzoneresource/latest/branch/reviewers/{PROJ}/{repo} 

**Project**: POST http://localhost:7990/rest/workzoneresource/latest/project/{PROJ}/reviewers

with the following JSON data payload (example)
```
{"projectKey”:"OTHER_PROJECT","repoSlug”:"OTHER_REPO","refName":"refs/heads/develop","users":[{"name":"agrant"}],"groups":["team-leads"],"mandatoryUsers":[],"mandatoryGroups":["mandatory-reviewers"],"topSuggestedReviewers":"1","daysInPast":0}
```

This way you can copy existing proj/repo/ref reviewer configurations to other proj/repos

### DELETE

To delete a  project/repo/branch reviewer configuration send a DELETE request of a **single** reviewers array entry.

**Repository**: DELETE http://localhost:7990/rest/workzoneresource/latest/branch/reviewers/{PROJ}/{repo}

**Project**: DELETE http://localhost:7990/rest/workzoneresource/latest/project/{PROJ}/reviewers

 with the following JSON data payload (example)

```
{"projectKey”:"OTHER_PROJECT","repoSlug”:"OTHER_REPO","refName":"refs/heads/develop","users":[{"name":"agrant"}],"groups":["team-leads"],"mandatoryUsers":[],"mandatoryGroups":["mandatory-reviewers"],"topSuggestedReviewers":"1","daysInPast":0}
```

### DELETE ALL

Delete the complete repository reviewer configuration. This represents a 'Reset to project configuration' action.

**Repository** DELETE http://localhost:7990/rest/workzoneresource/latest/branch/reviewerslist/{projectKey}/{repoSlug}

## Signed Approvals

Same GET/POST/DELETE

**Repository**: GET http://localhost:7990/rest/workzoneresource/latest/branch/signapprovers/{PROJ}/{repo}

**Project**: GET http://localhost:7990/rest/workzoneresource/latest/project/{PROJ}/signapprovers

Exampe GET ```[{"projectKey": "PROJ", "repoSlug": "repo", "refName": "refs/heads/develop", "users": [], "groups": ["develop-team"], "addAsReviewers": true}]```

Example POST `{"projectKey": "PROJ", "repoSlug": "repo", "refName": "refs/heads/develop", "users": [], "groups": ["develop-team"], "addAsReviewers": true}`

### DELETE ALL

Delete the complete repository sign-approver configuration. This represents a 'Reset to project configuration' action.

**Repository** DELETE http://localhost:7990/rest/workzoneresource/latest/branch/signapproverslist/{projectKey}/{repoSlug}


## Auto-Mergers

Same pattern as above, 

**Repository**: GET http://localhost:7990/rest/workzoneresource/latest/branch/automerge/{PROJ}/{repo} 

**Project**: GET http://localhost:7990/rest/workzoneresource/latest/project/{PROJ}/automerge

to get all branch auto-merge definitions as an array for a proj/repo. Then POST a **single** entry per branch back to server to modify or create a new proj/repo configuration.

### DELETE ALL

Delete the complete repository automerger configuration. This represents a 'Reset to project configuration' action.

**Repository** DELETE http://localhost:7990/rest/workzoneresource/latest/branch/automergelist/{projectKey}/{repoSlug}


## Workflow (Hooks)

The REST endpoint is: 

**Repository**: GET http://localhost:7990/rest/workzoneresource/latest/workflow/{PROJ}/{repo}

**Project**: GET http://localhost:7990/rest/workzoneresource/latest/workflow/{PROJ}

To enable/disable hooks POST the payload as

```
{"projectKey":"PROJ","repoSlug":"repo","pushAfterPullReq":false,"unapprovePullReq":false}
```

### DELETE

Delete the repository configuration. This represents a 'Reset to project configuration' action.

**Repository** DELETE http://localhost:7990/rest/workzoneresource/latest/workflow/{PROJ}/{repo}

The payload "projectKey" value must be equal to the /PROJ/ path parameter.
The payload "repoSlug" value must be equal to the /repo path parameter.

Note that workzone does not follow the common Stash REST endpoint pattern of `/project/{PROJ_KEY}/repo/{REPO_SLUG}`. Workzone assumes `/{PROJ_KEY}/{REPO_SLUG}`

 


